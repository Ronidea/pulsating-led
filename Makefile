CC = avr-gcc


.PHONY: all


%.o : %.c
	$(CC) -Os -DF_CPU=16000000UL -mmcu=atmega328p -c $< -o $@


main: main.o
	$(CC) -mmcu=atmega328p $< -o $@


upload: main
	avr-objcopy -O ihex -R .eeprom $< main.hex
	avrdude -F -V -c arduino -p ATMEGA328P -P /dev/ttyUSB0 -b 115200 -U flash:w:main.hex

	
