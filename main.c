#include <avr/io.h>
#include <util/delay.h>
#include <stdbool.h>

#include "pin_control.h"

#define ONE_MS 1
#define INPUT  0
#define OUTPUT 1

/*
 *	######################################
 *	#	ATmega328P SHORTCUTS: MEANINGS	 #
 *	######################################
 *
 *	TCNT:	Timer/Counter
 *	TTCR:	Timer/Counter Control Registe
 *	TOV:	Timer/Counter Overflow Flag
 *	TIFR:	Timer Interrupt Flag Register
 *	TIMSK:	Timer Interrupt Mask Register
 *	WGM02: 	Waveform Generator Mode
 *	COM:	Compare Output mode
 *	OCR:	Output Compare Registers
 *	OCF:	Output Compare Flag
 *	OC:		Output Compare
 *	FOC:	Force Output Compare
 *	CS:		Clock Select
 *	DDR:	Data Direction Register
 *
*/


void sleep(int ms);

int main (void) {
	bool cycle_up = true;

	PIN6_OUT;
	// datasheet page 113-119 for detailled explaination 
	TCCR0A = _BV(COM0A0) | _BV(COM0A1) | _BV(WGM00);
	TCCR0B = _BV(CS00);
	
 	while(1) {
		for(int i=0; i<=255; i++) {
			OCR0A = (cycle_up) ? i : 255-i;
			sleep(2);
		}
		cycle_up = !cycle_up;
	}
}

void sleep(int ms) {
	for (int i=0; i<ms; i++) {
		_delay_ms(ONE_MS);
	}
}
