#ifndef PIN_CONTROL_H_
#define PIN_CONTROL_H_
/*
 *  This file translates each digital pin and it's
 *  options to the respective register/bit
 *  
 */

// set to output mode
#define PIN0_OUT  DDRD |= _BV(DDD0)
#define PIN1_OUT  DDRD |= _BV(DDD1)
#define PIN2_OUT  DDRD |= _BV(DDD2)
#define PIN3_OUT  DDRD |= _BV(DDD3)
#define PIN4_OUT  DDRD |= _BV(DDD4)
#define PIN5_OUT  DDRD |= _BV(DDD5)
#define PIN6_OUT  DDRD |= _BV(DDD6)
#define PIN7_OUT  DDRD |= _BV(DDD7)
#define PIN8_OUT  DDRB |= _BV(DDB0)
#define PIN9_OUT  DDRB |= _BV(DDB1)
#define PIN10_OUT DDRB |= _BV(DDB2)
#define PIN11_OUT DDRB |= _BV(DDB3)
#define PIN12_OUT DDRB |= _BV(DDB4)
#define PIN13_OUT DDRB |= _BV(DDB5)

// set to input mode
#define PIN0_IN  DDRD &= ~_BV(DDD0)
#define PIN1_IN  DDRD &= ~_BV(DDD1)
#define PIN2_IN  DDRD &= ~_BV(DDD2)
#define PIN3_IN  DDRD &= ~_BV(DDD3)
#define PIN4_IN  DDRD &= ~_BV(DDD4)
#define PIN5_IN  DDRD &= ~_BV(DDD5)
#define PIN6_IN  DDRD &= ~_BV(DDD6)
#define PIN7_IN  DDRD &= ~_BV(DDD7)
#define PIN8_IN  DDRB &= ~_BV(DDB0)
#define PIN9_IN  DDRB &= ~_BV(DDB1)
#define PIN10_IN DDRB &= ~_BV(DDB2)
#define PIN11_IN DDRB &= ~_BV(DDB3)
#define PIN12_IN DDRB &= ~_BV(DDB4)
#define PIN13_IN DDRB &= ~_BV(DDB5)

// switch on
#define PIN0_ON  PORTD |= _BV(PORTD0)
#define PIN1_ON  PORTD |= _BV(PORTD1)
#define PIN2_ON  PORTD |= _BV(PORTD2)
#define PIN3_ON  PORTD |= _BV(PORTD3)
#define PIN4_ON  PORTD |= _BV(PORTD4)
#define PIN5_ON  PORTD |= _BV(PORTD5)
#define PIN6_ON  PORTD |= _BV(PORTD6)
#define PIN7_ON  PORTD |= _BV(PORTD7)
#define PIN8_ON  PORTB |= _BV(PORTB0)
#define PIN9_ON  PORTB |= _BV(PORTB1)
#define PIN10_ON PORTB |= _BV(PORTB2)
#define PIN11_ON PORTB |= _BV(PORTB3)
#define PIN12_ON PORTB |= _BV(PORTB4)
#define PIN13_ON PORTB |= _BV(PORTB5)

// switch off
#define PIN0_OFF  PORTD &= ~_BV(PORTD0)
#define PIN1_OFF  PORTD &= ~_BV(PORTD1)
#define PIN2_OFF  PORTD &= ~_BV(PORTD2)
#define PIN3_OFF  PORTD &= ~_BV(PORTD3)
#define PIN4_OFF  PORTD &= ~_BV(PORTD4)
#define PIN5_OFF  PORTD &= ~_BV(PORTD5)
#define PIN6_OFF  PORTD &= ~_BV(PORTD6)
#define PIN7_OFF  PORTD &= ~_BV(PORTD7)
#define PIN8_OFF  PORTB &= ~_BV(PORTB0)
#define PIN9_OFF  PORTB &= ~_BV(PORTB1)
#define PIN10_OFF PORTB &= ~_BV(PORTB2)
#define PIN11_OFF PORTB &= ~_BV(PORTB3)
#define PIN12_OFF PORTB &= ~_BV(PORTB4)
#define PIN13_OFF PORTB &= ~_BV(PORTB5)

#endif